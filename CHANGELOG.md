# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2020-06-17
### Added
- Second pallete for Hidden Gems in vi.lib in LV2020.

## [0.1.0] - 2020-06-16
### Added
- Initial version for vi.lib in LV2019.
