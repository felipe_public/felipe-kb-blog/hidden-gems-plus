# Hidden Gems Plus
Aditional VIs for LabVIEW 2019 and 2020 not included in original Hidden Gems Package

## Usage
- The VIs are available in the pallete Hidden Gems Plus.

## Dependencies
There are no Dependencies

## Author
- Felipe Pinheiro Silva